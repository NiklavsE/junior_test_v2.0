<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mydb";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
 
/* database structure: 
CREATE TABLE `products` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `SKU` varchar(20) NOT NULL,
 `Name` varchar(50) CHARACTER SET latin1 NOT NULL,
 `Price` int(11) NOT NULL,
 `Spec_attr` varchar(50) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8
*/

?>