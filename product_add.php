<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<!-- script for bootstrap buttons and better looking table -->
 <body>

<h1> Product Add</h1>
<!-- form containing the neccessary values for submission into sql database -->
<table>
<form method="POST" action='' > 
<tr><td>
SKU:
</td><td> <input type="text" name="SKU" /> 
</td></tr><tr><td>
Name:
</td><td> <input type="text" name="Name"/>
</td></tr><tr><td>
Price:
</td><td> <input type="text" name="Price"/> 
</td></tr><tr><td>
Type:
</td><td>
<select name="cases" id="cases">
  <option value="" >-Select a type-</option>
  <option value="CD" >CD</option>
  <option  value="W">Books</option>
  <option  value="D">Furniture</option>
</select>
</td></tr>
</table>

<!-- dynamic listbox based on select 'cases' values -->
<div class="CD" id="CD">
<table border="3"> 
<tr><td>
Size:</td><td><input type='text' name='size' ></td> 
</tr></table>
Please enter the size of the disc in MB
</div>

<div class="W" id="W">
<table border="3"> 
<tr><td>
Weight:</td><td><input type='text' name='weight' ></td> 
</tr></table>
Please enter the weight of the book in kilograms
</div>

<div class="D" id="D">
<table border="3"> 
<tr><td>
Height:</td><td><input type='text' name='height' ></td></tr>
<tr><td>
Width:</td><td><input type='text' name='width' ></td></tr>
<tr><td>
Length:</td><td><input type='text' name='length' ></td>
</tr></table>
Please enter dimensions for the furniture
</div>
<!-- jquery script that operates with the listbox values
 and based on selection displays correct table -->
<script type="text/javascript"
 src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
$('div').hide()
$('#cases').change(function () {
	  var value = this.value;
    $('div').hide()
    $('#' + this.value).show();
});
</script>
<input type="submit" name='Save' value='save' class="btn btn-success">
</form>  


<?php 
// cass contains 2 classes and its functions 
// database_con is to establish db connection
include 'database_con.php'; 
include 'class.php';

if (isset($_POST['Save'])){ 
	$product = new Products_add($_POST['SKU'],$_POST['Name'],$_POST['Price']);
	// class function to properly add either mb/kg/dimension seperators
	$product ->format(); 
	// verifiy function, to check if all the fields are filled
	if ($product->verify() == false) {
		echo 'Error processing, please fill out all the fields!';
		exit;
	} 
	// if everything is filled, then creates a sql statement
	else 
	{
	$sql = $product->create_sql_statement();
	// and inserts it into query, on success displays that new record is created
	if ($conn->query($sql) === TRUE) 
	{
		echo "New record created successfully";
	} 
	else 
	{
		// display error message, if failed 
		echo "Error: " . $sql . "<br>" . $conn->error;
	}
	// closes connection
	$conn->close();
	} 
}
?>
</body>
</html>