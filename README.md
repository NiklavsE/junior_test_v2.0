Niklāvs Emīls Eglītis. My take on Junior Developers test v2.0. 
product_list.php file contains the first part of the task. It displays saved products in mysql database. 
product_add.php file contains the second part - a form for creating products. 
database_con.php is database related. Contains the connection credentials, and also the database product table structure. 
class.php contains two classes. One for displaying fetched products from database and the second for creating products and applying 
them to database.